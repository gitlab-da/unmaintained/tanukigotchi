declare namespace Express {
  import { RequestContext } from 'express-openid-connect';
  interface Request {
    // customProperties: string[];
    // rawBody: any;
    oidc: RequestContext;
  }
}
