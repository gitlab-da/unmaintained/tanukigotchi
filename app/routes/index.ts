import { Request, Response } from 'express';

import { getTanukiFact, getTanukiAdjective } from '../lib/tanuki';
import * as appRoutes from './app';

export const homepage = (req: Request, res: Response) => {
  if (req.oidc.isAuthenticated()) res.redirect('/app');
  res.render('home', {
    tanukiadj: getTanukiAdjective(),
    tanukifact: getTanukiFact(),
  });
};

export const about = (req: Request, res: Response) => {
  res.render('about');
};

export const profile = (req: Request, res: Response) => {
  console.log(req.oidc.user);
  res.render('profile');
};

export const app = appRoutes;
