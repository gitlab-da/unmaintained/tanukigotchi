/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Request, Response } from 'express';
import { ensureUser } from '../lib/users';

export const apphome = async (req: Request, res: Response) => {
  ensureUser(req.oidc.user)
  .then((user) => {
    console.log('user', user);
    res.render('app/home', { user });
  })
  .catch((err) => {
    console.error(err);
    res.status(500).send('Error getting user');
  })
  // try {
  //   const user: User = await ensureUser(req.oidc.user);
  // } catch (error) {
  //   res.status(500).send('Error getting user');
  // }
};
