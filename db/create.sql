CREATE TABLE IF NOT EXISTS "users" (
	"id" serial,
	"email" text,
	"nickname" text,
	"name" text,
    "picture" text,
    "initialized" boolean,
	PRIMARY KEY( id )
);

